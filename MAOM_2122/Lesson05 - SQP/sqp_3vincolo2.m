clear all;
close all;
clc;

global iter;
iter = zeros(0,3);

% Mappa punti e plot
x = (-3:0.1:3);
y = (-3:0.1:3);
val = zeros(numel(y),numel(x));
for k=1:numel(x)
    for m=1:numel(y);
        val(m,k) = rosenboth([x(k),y(m)]);
    end
end
surf(x,y,val); hold on;
plot3(x(x==1),y(y==1),val(y==1,x==1)+10,'-og','MarkerSize',12);
view([0 0 1]);
axis([min(x) max(x) min(y) max(y)]);

%% Vincoli lineari --------------------------------------------------------
% Vincolo 1
pts1 = [0  1; ...
        3 -3];
m1 = (pts1(2,2)-pts1(1,2))/(pts1(2,1)-pts1(1,1));
q1 = -pts1(1,1)*(pts1(2,2)-pts1(1,2))/(pts1(2,1)-pts1(1,1))+pts1(1,2);
yPts1 = m1*x+q1;
plot3(x,yPts1,max(max(val))*ones(size(x)),'-g');
A(1,:) = ([pts1(2,2)-pts1(1,2) pts1(1,1)-pts1(2,1)])    *-1;
b(1) = (pts1(1,1)*(pts1(2,2)-pts1(1,2))-pts1(1,2)*(pts1(2,1)-pts1(1,1)))   *-1;

% Vincolo 2
pts2 = [-3  -3; ...
        0 3];
m2 = (pts2(2,2)-pts2(1,2))/(pts2(2,1)-pts2(1,1));
q2 = -pts2(1,1)*(pts2(2,2)-pts2(1,2))/(pts2(2,1)-pts2(1,1))+pts2(1,2);
yPts2 = m2*x+q2;
plot3(x,yPts2,max(max(val))*ones(size(x)),'-g');
A(2,:) = ([pts2(2,2)-pts2(1,2) pts2(1,1)-pts2(2,1)])   *-1;
b(2) = (pts2(1,1)*(pts2(2,2)-pts2(1,2))-pts2(1,2)*(pts2(2,1)-pts2(1,1)))   *-1;

% Ottimizzatore
lb = [-3;-3];
ub = [3;3];
% x0 = [0;-3];
x0 = [-3;-3];
opts = optimset('Algorithm', 'sqp','OutputFcn',@optimplot3d);
[x,fval] = fmincon(@(x) rosenboth(x),x0,A,b,[],[],lb,ub,[],opts);
% [x,fval] = fmincon(@(x) rosenboth(x),x0,A,b,[],[],lb,ub,[],opts);
plot3(x(1),x(2),fval,'wo','MarkerSize',15);