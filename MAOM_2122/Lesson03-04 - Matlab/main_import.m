clear 
close all
clc

%% IMPORTDATA -------------------------------------------------------------
filename = 'data_days.txt';
delimiterIn = ' ';
headerlinesIn = 1;
A = importdata(filename,delimiterIn,headerlinesIn);

% View data
for k = [1:7]
   disp(A.colheaders{1, k})
   disp(A.data(:, k))
   disp(' ')
end

%% FPRINTF - FSCANF -------------------------------------------------------
clear; close all; clc;

a = (1:5);
b = (2:2:10);

fid = fopen('dati.txt','w+');
for k=1:numel(a)
    fprintf(fid,'i numeri sono %5.3f e %5.3f\n',a(k),b(k));
end
fclose(fid);

fid = fopen('dati.txt','r');
A = fscanf(fid,'%*s %*s %*s %f %*s %f',[2 Inf]);
fclose(fid);


%% FSCANF -------------------------------------------------------
clear; close all; clc;
filename = 'data_rain.txt';
rows = 7;
cols = 5;
 
% open the file
fid = fopen(filename);
 
% read the file headers, find M (number of months)
M = fscanf(fid, '%*s %*s\n%*s %*s %*s %*s\nM=%d\n\n', 1);
 
% read each set of measurements
for n = 1:M
   mydata(n).time = fscanf(fid, '%s', 1);
   mydata(n).month = fscanf(fid, '%s', 1);
 
   % fscanf fills the array in column order,
   % so transpose the results
   mydata(n).raindata  = ...
      fscanf(fid, '%f', [rows, cols]);
end
for n = 1:M
   disp(mydata(n).time), disp(mydata(n).month)
   disp(mydata(n).raindata)
end
 
% close the file
fclose(fid);