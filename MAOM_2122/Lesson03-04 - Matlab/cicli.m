clear
close all
clc

%% ciclo for 
for ii=1:10
    stringa=['la variabile � ',num2str(ii)];
    disp(stringa)
end

%% ciclo for annidato
ind=0;
A=zeros(5);
for ii=1:5
    for jj=1:5
        ind=ind+1;
        A(ii,jj)=ind;
    end
end

%% ciclo while
ii=0;
while(ii<10)
    disp i
    ii=ii+1;
end

%% switch - otherwise
switch(2+3)
    case{2,3,4}
        x=-1;
    case 5
        x=0;
    otherwise x=2;
end

